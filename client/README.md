# Matomo tracker client example

## Get started
Add tracking script
~~~
<script type="text/javascript" src="dist/k2m-tr.min.js"></script>
~~~

Configure tracking
~~~
    <script type="text/javascript">
        configureTracker({
            clientID: 'testClientID',
            userID: 'testUserID',
            siteID: 'testSiteID',
            trackingServerURL: 'https://tracking.key2market.com',
            enableLinkTracking: true,
            enableTrackPageView: true
        });
    </script>
~~~

**clientID:** Required

**userID:** Optional

**siteID:** Required

**trackingServerURL:** Required

**enableTrackPageView:** Boolean. True by default. Send page view action on page load.  

**enableLinkTracking:** Boolean. True by default. Send link action on click by link.

##Run test client

~~~
python -m SimpleHTTPServer 8000
~~~

Open browser
http://localhost:8000/


##Deployment

~~~
npm run build
aws s3 sync ./dist s3://titan-tracking-staticfiles
~~~
