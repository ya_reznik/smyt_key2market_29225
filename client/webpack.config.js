const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        'k2m-tr': './src/index.js',
        piwik: './src/piwik.js'
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'dist')
    }
};
