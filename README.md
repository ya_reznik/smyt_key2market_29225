# Beacon Web Server and Matomo JS Tracking Client
## General information
The task is to adapt AWS Beacon Web Server (https://docs.aws.amazon.com/en_us/solutions/latest/real-time-web-analytics-with-kinesis/overview.html) for Matomo JS Tracking Client (https://developer.matomo.org/guides/tracking-javascript-guide).

The metrics to be logged are:

## Required parameters for all clients:

 - event_id
 - session_id
 - idsite  — The ID of the website we're tracking a visit/action for. (com.key2market.app for example)
 - rec — Required for tracking, must be set to one, eg, &rec=1.
 - action_name - [page_view, click, context, outlink, download, search]
 - url  — The full URL for the current action.
 - rand — Meant to hold a random value that is generated before each request. Using it helps avoid the tracking
 - request being cached by the browser or a proxy.
 - apiv — The parameter &apiv=1 defines the api version to use (currently always set to 1)
 - ua — An override value for the User-Agent HTTP header field. The user agent is used to detect the operating system and browser used.
 - lang — An override value for the Accept-Language HTTP header field. This value is used to detect the visitor's country if GeoIP is not enabled.
 - pv_id — Accepts a six character unique ID that identifies which actions were performed on a specific page view. When a page was viewed, all following tracking requests (such as events) during that page view should use the same pageview ID. Once another page was viewed a new unique ID should be generated. Use [0-9a-Z] as possible characters for the unique ID.
 - request - original request
 - referrer
 - user fingerprint
 - session identification
 - custom variables

##Optional: For K2M Application

 - uid — Cognito User ID
 - cid — Cognito Client ID
 - e_c — The event category. Must not be empty. (eg. Videos, Music, Games...)
 - e_a — The event action. Must not be empty. (eg. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)
 - e_n — The event name. (eg. a Movie name, or Song name, or File name...)
 - e_v — The event value. Must be a float or integer value (numeric), not a string.

AWS services management implies the usage of templates for the services or applications setup. The services and applications we use are called “stack”. Stacks can be updated or replicated as needed. The template describes everything your service is supposed to do including custom resources. Template is a JSON or YAML format file (https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/template-guide.html).

The AWS Beacon Server includes two main code components to process metrics. The first one is a custom python code that receives requests from javascript tracking client and put it to the log file. We have deliberately used the python app instead of Apache server AWS suggests by default. Custom python app made the service more flexible to meet Matomo JS Tracking Client features.

The second code component is default AWS kinesis-agent. Kinesis-agent is monitoring log files and sends new data to the Amazon Kinesis Firehose. Finally the data are saved to AWS S3.

You may find the actual template at https://bitbucket.org/ya_reznik/smyt_key2market_29225/src/master/kinesis.template

We used the default template and made some changes that are easy to trace (see line 375 and below).

##Setting up AWS CloudFormation stack
1) Get your template ready, create a CloudFormation stack and upload the template to CloudFormation dashboard

![picture](img/doc4.png)

2) Specify stack details and options

![picture](img/doc5.png)

3) Create stack

4) Once stack is created please find BeaconServerUrl in Output section

![picture](img/doc3.png)

and S3 Buckets, EC2 instances, etc in Resources section. AnalyticsBucket stores the metrics sent by Matomo JS Tracking Client.
The events sent by Matomo JS Tracking Client are stored to S3 in JSON format.

##Setting up client tracker

Add `k2m-tr.min.js` JavaScript tracking code to your website code:

`<script src='http://tracking.key2market.com/static/k2m-tr.min.js'></script>`

Тhen add the call like this:

`configureTracker(userId, clientID, siteID, beaconServerURL);`

 - userId - your website user's identifier
 - clientID - tracking system identifier
 - siteID - website identifier
 - beaconServerURL - tracking server endpoint

Example:

`configureTracker('SomeUserID', '1f28a393-f63e-4c66-82d9-bd6b7ab464b7', 'key2market.com', 'https://tracking.key2market.com');`

That will track all 'open page' events.

If you need to add 'click button' event to the tracker, please, add something like this to your 'Click me' button code:

`<button onclick="_paq.push(['trackEvent', action, [name]]);">Click me</button>`

 - action - action type
 - name - additional information
 - value - additional information

Example:

`<button onclick="_paq.push(['trackEvent', 'button_click', 'Get a Quote']);">GET A QUOTE</button>`
