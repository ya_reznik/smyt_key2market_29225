import os
import logging
import uuid
from datetime import datetime
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from flask import Flask, request


LOG_DIR = os.getenv('LOG_DIR', os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs'))

# Create log dir if it doesn't exist
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

log = logging.getLogger('stream')

# Rotating logfile path and rotation parameters
handler = RotatingFileHandler(os.path.join(LOG_DIR, 'beacon_log.txt'), maxBytes=100 * 1024 * 1024, backupCount=10)
log.addHandler(handler)
log.setLevel(logging.INFO)

app = Flask(__name__)


def get_action_type(params):
    if 'action_name' in params:
        return 'page_view'
    if 'link' in params:
        return 'link'
    if 'e_c' in params:
        return params.get('e_c')
    return 'custom'


def format_log(record):
    values = []
    for value in record.values():
        if value is None or value == '':
            values.append('-')
        else:
            values.append("{}".format(value))
    return " ".join(map(lambda x: "{}".format(x), values))


@app.route('/beacon/')
def beacon():
    # Copy all HTTP GET parameters to args
    args = request.args.copy()

    # Map of all values in logfile
    record = OrderedDict([
        ('datetime', datetime.utcnow().isoformat()),  # Datetime of request in UTC timezone
        ('event_id', str(uuid.uuid1())),
        ('action_type', get_action_type(args)),
        ('idsite', args.get('idsite')),
        ('rec', args.get('rec', '1')),
        ('rand', args.get('rand')),
        ('url', args.get('url')),
        ('uid', args.get('uid')),
        ('cid', args.get('cid')),
        ('e_c', args.get('e_c')),
        ('e_a', args.get('e_a')),
        ('e_n', args.get('e_n')),
        ('e_v', args.get('e_v')),
        ('apiv', args.get('apiv', '1')),
        ('ua', args.get('ua')),
        ('lang', args.get('lang')),
        ('pv_id', args.get('pv_id')),
        ('session_id', args.get('session')),  # Session from tracker
        ('ip', request.headers.getlist('X-Forwarded-For')[0] if request.headers.getlist("X-Forwarded-For") else request.remote_addr),  # IP address of remote user
        ('request', request.full_path),  # full URI path of request
        ('referrer', request.referrer if request.referrer else '-'),  # HTTP referrer which was sent in headers. '-' if it's empty
        ('custom_values', args.get('_cvar'))  # Custom variables as a JSON string
    ])

    # save log record
    log.info(format_log(record))
    return 'OK'


if __name__ == '__main__':
    app.run('0.0.0.0', port=80)
